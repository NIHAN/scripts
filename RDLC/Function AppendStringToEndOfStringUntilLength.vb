Public Function AppendStringToEndOfStringUntilLength(ByVal Value As String, ByVal StringToAdd As String, ByVal UntilLength As Integer)
    While (Value.Length < UntilLength)
        Value += StringToAdd
    End While
    Return Value
End Function