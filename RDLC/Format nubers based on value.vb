'1. Create Function in Code property

Public Function GetFormattingBasedOnValue(ByVal Value As Decimal)
    if (Round(Value, 1) - Floor(Value)) = 0 then
        Return "#,0;-#,0;''"
    end if
    Return "#,0.0;-#,0.0;''"
End Function

'2. Name the TextBox you wish to format

'3. Write the format expression

Code.GetFormattingBasedOnValue(ReportItems!{Your TextBox Name}.Value)