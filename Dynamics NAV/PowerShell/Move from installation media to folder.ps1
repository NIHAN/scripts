Add-Type -AssemblyName System.Windows.Forms
$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
    InitialDirectory = [Environment]::GetFolderPath('Desktop')
    Filter = 'Compressed folder (*.zip)|*.zip'
}
$null = $FileBrowser.ShowDialog()

$DynamicsNAVInstallation = $FileBrowser.FileName
$ZipFileCopyLocation = "C:\Temp"

if (($DynamicsNAVInstallation -eq "") -or !(Test-Path $DynamicsNAVInstallation -PathType Leaf)) {
     throw [System.IO.FileNotFoundException] "File not found."
}

if (!(Test-Path $ZipFileCopyLocation)) {
    $null = New-Item -ItemType Directory -Path $ZipFileCopyLocation
}

$UnZippedInstallationFolder = Join-Path $ZipFileCopyLocation (Get-Item $DynamicsNAVInstallation).BaseName
if (!(Test-Path $UnZippedInstallationFolder)) {
    $null = New-Item -ItemType Directory -Path $UnZippedInstallationFolder
}

Function Unzip {
    [cmdletbinding()]
    param (
        [Parameter(Mandatory=$True)]
        [string]$ZipFile,
        [Parameter(Mandatory=$True)] 
        [string]$OutPath
    )
    Process {
        if (!(Test-Path $OutPath)) {
            $null = New-Item -ItemType Directory -Path $OutPath
        }
        Add-Type -AssemblyName System.IO.Compression.FileSystem
        [System.IO.Compression.ZipFile]::ExtractToDirectory($ZipFile, $OutPath)
    }
}

Write-Output "Unpacking installationfolder"
Unzip -ZipFile $DynamicsNAVInstallation -OutPath $UnZippedInstallationFolder

$NAVVersion = [System.Diagnostics.FileVersionInfo]::GetVersionInfo((Join-Path $UnZippedInstallationFolder "Setup\CFiles\Microsoft Dynamics NAV\80\Setup\setup.exe")).FileVersion
$SplittedNAVVersion = $NAVVersion.Split('.')

Function Create-NAVFolderStructure {
    [cmdletbinding()]
    param (
        [Parameter(Mandatory=$True)]
        [int]$MajorVersionNo,
        [Parameter(Mandatory=$True)]
        [int]$MinorVersionNo,
        [Parameter(Mandatory=$True)] 
        [int]$BuildNo
    )
    Process {
        $BaseFolder = Join-Path "C:\Dynamics NAV\" $MajorVersionNo 
        $BaseFolder = Join-Path $BaseFolder "$MajorVersionNo.$MinorVersionNo.$BuildNo"
        if (!(Test-Path $BaseFolder)) {
            $null = New-Item -ItemType Directory -Path $BaseFolder
        }
        $RTCFolder = Join-Path $BaseFolder "RoleTailoredClient"
        $NSTFolder = Join-Path $BaseFolder "Service"
        if (!(Test-Path $RTCFolder)) {
            $null = New-Item -ItemType Directory -Path $RTCFolder
        }
        if (!(Test-Path $NSTFolder)) {
            $null = New-Item -ItemType Directory -Path $NSTFolder
        }
        return $BaseFolder
    }    
}

Write-Output "Creating folder structur"
$BaseFolder = Create-NAVFolderStructure -MajorVersionNo $SplittedNAVVersion[0] -MinorVersionNo $SplittedNAVVersion[1] -BuildNo $SplittedNAVVersion[2]

Function Move-NAVInstallationFolders {
    [cmdletbinding()]
    param (
        [Parameter(Mandatory=$True)]
        [string]$BaseInstallationFolder,
        [Parameter(Mandatory=$True)]
        [string]$UnZippedInstallationFolder
    )
    Process {
        $RTCFolder = Join-Path $BaseInstallationFolder "RoleTailoredClient"
        $NSTFolder = Join-Path $BaseInstallationFolder "Service"
        $RTCUnzippedFolder = Join-Path $UnZippedInstallationFolder "RoleTailoredClient\program files\Microsoft Dynamics NAV\80\RoleTailored Client"
        $NSTUnzippedFolder = Join-Path $UnZippedInstallationFolder "ServiceTier\program files\Microsoft Dynamics NAV\80\Service"
        $InstallersUnzippedRTCFolder = Join-Path $UnZippedInstallationFolder "Installers\DK\RTC\PFiles\Microsoft Dynamics NAV\80\RoleTailored Client"
        $InstallersUnzippedNSTFolder = Join-Path $UnZippedInstallationFolder "Installers\DK\Server\PFiles\Microsoft Dynamics NAV\80\Service"
        Get-ChildItem -Path $RTCUnzippedFolder | Move-Item -Destination $RTCFolder -Force
        Get-ChildItem -Path $NSTUnzippedFolder | Move-Item -Destination $NSTFolder -Force
        Get-ChildItem -Path $InstallersUnzippedRTCFolder -Recurse -File | 
            ForEach-Object {
                $MoveTo = Join-Path $RTCFolder $_.FullName.Replace($InstallersUnzippedRTCFolder,"")
                $MoveToPath = Split-Path $MoveTo
                if (!(Test-Path $MoveToPath)) {
                    $null = New-Item -ItemType Directory -Path $MoveToPath
                }
                Move-Item -Path $_.FullName -Destination $MoveToPath -Force
            }
        Get-ChildItem -Path $InstallersUnzippedNSTFolder -Recurse -File | 
            ForEach-Object {
                $MoveTo = Join-Path $NSTFolder $_.FullName.Replace($InstallersUnzippedNSTFolder,"")
                $MoveToPath = Split-Path $MoveTo
                if (!(Test-Path $MoveToPath)) {
                    $null = New-Item -ItemType Directory -Path $MoveToPath
                } 
                Move-Item -Path $_.FullName -Destination $MoveToPath -Force
            }
    }
}

Write-Output "Moving installations"
Move-NAVInstallationFolders -BaseInstallationFolder $BaseFolder -UnZippedInstallationFolder $UnZippedInstallationFolder

Write-Output "Removing unzipped folder"
Remove-Item $UnZippedInstallationFolder -Recurse -Force

Write-Output "Finish"